import { panchMahapurushYogList, dummyYogList } from "./enum.js";

const calculate = (chart) => {
  var yogsFound = [];
  // First review existence of Panch Maha Purush Yog List
  for (const hz in chart) {
      if ((chart[hz].house === 1 || chart[hz].house === 4 || chart[hz].house === 7 || chart[hz].house === 10) && chart[hz].planets.length) {
          for (const plnt in chart[hz].planets) {
              switch (chart[hz].planets[plnt].name) {
                  case "Ma":
                      if (chart[hz].planets[plnt].sign === 1 || chart[hz].planets[plnt].sign === 8 || chart[hz].planets[plnt].sign === 10) yogsFound.push(panchMahapurushYogList.ruchika_yog);
                      break;
                  case "Me":
                      if (chart[hz].planets[plnt].sign === 3 || chart[hz].planets[plnt].sign === 6) yogsFound.push(panchMahapurushYogList.bhadra_yog);
                      break;
                  case "Ju":
                      if (chart[hz].planets[plnt].sign === 4 || chart[hz].planets[plnt].sign === 9 || chart[hz].planets[plnt].sign === 12) yogsFound.push(panchMahapurushYogList.hamsa_yog);
                      break;
                  case "Ve":
                      if (chart[hz].planets[plnt].sign === 2 || chart[hz].planets[plnt].sign === 7 || chart[hz].planets[plnt].sign === 12) yogsFound.push(panchMahapurushYogList.malavya_yog);
                      break;
                  case "Sa":
                      if (chart[hz].planets[plnt].sign === 7 || chart[hz].planets[plnt].sign === 10 || chart[hz].planets[plnt].sign === 11) yogsFound.push(panchMahapurushYogList.shasha_yog);
                      break;
              };
          }
      }
  };
  return yogsFound;
};




export {calculate};