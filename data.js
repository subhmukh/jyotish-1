const chart1 =
[
    {
      "house": 1,
      "sign": 11,
      "zodiac": "Aquarius",
      "planets": [
        {
          "name": "As",
          "degree": "5.32",
          "nakshatra": "Dhanista",
          "pada": 4,
          "house": 1,
          "sign": 11,
          "isWith": [
            {
              "name": "Sa"
            }
          ]
        },
        {
          "name": "Sa",
          "degree": "19.06",
          "nakshatra": "Shatabhi",
          "pada": 4,
          "state": "mooltrikona",
          "combust": false,
          "house": 1,
          "sign": 11,
          "isWith": []
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Sa",
      "lordHouse": 1,
      "lordSign": 11,
      "lordWith": [],
      "bhinashtakvarga": {
        "Su": 3,
        "Mo": 3,
        "Ma": 4,
        "Me": 4,
        "Ju": 5,
        "Ve": 4,
        "Sa": 1
      },
      "sarvashtakvarga": 24
    },
    {
      "house": 2,
      "sign": 12,
      "zodiac": "Pisces",
      "planets": [
        {
          "name": "Mo",
          "degree": "29.70",
          "nakshatra": "Revati",
          "pada": 4,
          "state": "neutral",
          "combust": false,
          "house": 2,
          "sign": 12,
          "isWith": []
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Ju",
      "lordHouse": 5,
      "lordSign": 3,
      "lordWith": [],
      "bhinashtakvarga": {
        "Su": 3,
        "Mo": 5,
        "Ma": 2,
        "Me": 4,
        "Ju": 4,
        "Ve": 6,
        "Sa": 2
      },
      "sarvashtakvarga": 26
    },
    {
      "house": 3,
      "sign": 1,
      "zodiac": "Aries",
      "planets": [],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Ma",
      "lordHouse": 12,
      "lordSign": 10,
      "lordWith": [
        {
          "name": "Ve"
        }
      ],
      "bhinashtakvarga": {
        "Su": 4,
        "Mo": 4,
        "Ma": 5,
        "Me": 6,
        "Ju": 5,
        "Ve": 6,
        "Sa": 4
      },
      "sarvashtakvarga": 34
    },
    {
      "house": 4,
      "sign": 2,
      "zodiac": "Taurus",
      "planets": [
        {
          "name": "Ra",
          "degree": "9.28",
          "nakshatra": "Krittika",
          "pada": 4,
          "state": "",
          "house": 4,
          "sign": 2,
          "isWith": []
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Ve",
      "lordHouse": 12,
      "lordSign": 10,
      "lordWith": [
        {
          "name": "Ma"
        }
      ],
      "bhinashtakvarga": {
        "Su": 3,
        "Mo": 6,
        "Ma": 4,
        "Me": 5,
        "Ju": 2,
        "Ve": 5,
        "Sa": 4
      },
      "sarvashtakvarga": 29
    },
    {
      "house": 5,
      "sign": 3,
      "zodiac": "Gemini",
      "planets": [
        {
          "name": "Ju",
          "degree": "1.02",
          "nakshatra": "Mrigashir",
          "pada": 3,
          "state": "neutral",
          "combust": false,
          "house": 5,
          "sign": 3,
          "isWith": []
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Me",
      "lordHouse": 10,
      "lordSign": 8,
      "lordWith": [
        {
          "name": "Ke"
        }
      ],
      "bhinashtakvarga": {
        "Su": 2,
        "Mo": 5,
        "Ma": 1,
        "Me": 1,
        "Ju": 5,
        "Ve": 4,
        "Sa": 5
      },
      "sarvashtakvarga": 23
    },
    {
      "house": 6,
      "sign": 4,
      "zodiac": "Cancer",
      "planets": [],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Mo",
      "lordHouse": 2,
      "lordSign": 12,
      "lordWith": [],
      "bhinashtakvarga": {
        "Su": 5,
        "Mo": 4,
        "Ma": 2,
        "Me": 3,
        "Ju": 7,
        "Ve": 3,
        "Sa": 4
      },
      "sarvashtakvarga": 28
    },
    {
      "house": 7,
      "sign": 5,
      "zodiac": "Leo",
      "planets": [],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Su",
      "lordHouse": 11,
      "lordSign": 9,
      "lordWith": [],
      "bhinashtakvarga": {
        "Su": 5,
        "Mo": 2,
        "Ma": 4,
        "Me": 6,
        "Ju": 5,
        "Ve": 1,
        "Sa": 2
      },
      "sarvashtakvarga": 25
    },
    {
      "house": 8,
      "sign": 6,
      "zodiac": "Virgo",
      "planets": [],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Me",
      "lordHouse": 10,
      "lordSign": 8,
      "lordWith": [
        {
          "name": "Ke"
        }
      ],
      "bhinashtakvarga": {
        "Su": 4,
        "Mo": 6,
        "Ma": 3,
        "Me": 5,
        "Ju": 5,
        "Ve": 5,
        "Sa": 2
      },
      "sarvashtakvarga": 30
    },
    {
      "house": 9,
      "sign": 7,
      "zodiac": "Libra",
      "planets": [],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Ve",
      "lordHouse": 12,
      "lordSign": 10,
      "lordWith": [
        {
          "name": "Ma"
        }
      ],
      "bhinashtakvarga": {
        "Su": 5,
        "Mo": 3,
        "Ma": 3,
        "Me": 5,
        "Ju": 4,
        "Ve": 6,
        "Sa": 4
      },
      "sarvashtakvarga": 30
    },
    {
      "house": 10,
      "sign": 8,
      "zodiac": "Scorpio",
      "planets": [
        {
          "name": "Me",
          "degree": "27.33",
          "nakshatra": "Jyeshtha",
          "pada": 4,
          "state": "neutral",
          "combust": false,
          "house": 10,
          "sign": 8,
          "isWith": [
            {
              "name": "Ke"
            }
          ]
        },
        {
          "name": "Ke",
          "degree": "9.28",
          "nakshatra": "Anuradha",
          "pada": 2,
          "state": "",
          "house": 10,
          "sign": 8,
          "isWith": [
            {
              "name": "Me"
            }
          ]
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Ma",
      "lordHouse": 12,
      "lordSign": 10,
      "lordWith": [
        {
          "name": "Ve"
        }
      ],
      "bhinashtakvarga": {
        "Su": 4,
        "Mo": 4,
        "Ma": 5,
        "Me": 7,
        "Ju": 5,
        "Ve": 5,
        "Sa": 4
      },
      "sarvashtakvarga": 34
    },
    {
      "house": 11,
      "sign": 9,
      "zodiac": "Sagitarious",
      "planets": [
        {
          "name": "Su",
          "degree": "16.94",
          "nakshatra": "P.Shadha",
          "pada": 2,
          "state": "neutral",
          "house": 11,
          "sign": 9,
          "isWith": []
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Ju",
      "lordHouse": 5,
      "lordSign": 3,
      "lordWith": [],
      "bhinashtakvarga": {
        "Su": 5,
        "Mo": 4,
        "Ma": 3,
        "Me": 3,
        "Ju": 4,
        "Ve": 3,
        "Sa": 5
      },
      "sarvashtakvarga": 27
    },
    {
      "house": 12,
      "sign": 10,
      "zodiac": "Capricorn",
      "planets": [
        {
          "name": "Ma",
          "degree": "13.66",
          "nakshatra": "Sravana",
          "pada": 2,
          "state": "neutral",
          "combust": false,
          "house": 12,
          "sign": 10,
          "isWith": [
            {
              "name": "Ve"
            }
          ]
        },
        {
          "name": "Ve",
          "degree": "20.04",
          "nakshatra": "Sravana",
          "pada": 4,
          "state": "neutral",
          "combust": false,
          "house": 12,
          "sign": 10,
          "isWith": [
            {
              "name": "Ma"
            }
          ]
        }
      ],
      "transits": {
        "date": "2022-02-20T21:51:38.124Z",
        "planets": []
      },
      "lord": "Sa",
      "lordHouse": 1,
      "lordSign": 11,
      "lordWith": [],
      "bhinashtakvarga": {
        "Su": 5,
        "Mo": 3,
        "Ma": 3,
        "Me": 5,
        "Ju": 5,
        "Ve": 4,
        "Sa": 2
      },
      "sarvashtakvarga": 27
    }
  ]

  export {chart1};