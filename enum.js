

const panchMahapurushYogList = {
    // When 5 of non-luminary core planets are in angular houses [1,4,7,10] from Ascendant, AND also in OWN house or EXALTED house
    ruchika_yog: {
        title: "Ruchaka Yoga",
    },
    bhadra_yog: {
        title: "Bhadra Yoga",
    },
    hamsa_yog: {
        title: "Hamsa Yoga",
    },
    malavya_yog: {
        title: "Malavya Yoga",
    },
    shasha_yog: {
        title: "Shasha Yoga",
    }
};

const dummyYogList = {
    // When 5 of non-luminary core planets are in angular houses [1,4,7,10] from Ascendant, AND also in OWN house or EXALTED house
    dummy_one: {
        title: "dummy yog 1",
    },
    dummy_two: {
        title: "dummy yog 2",
    }
};


export {panchMahapurushYogList, dummyYogList}